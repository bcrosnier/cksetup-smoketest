﻿using CK.Core;

namespace CKSetup.Test.DB
{
    [SqlPackage(
         ResourcePath = "Res",
         Schema = "TEST",
         Database = typeof(SqlDefaultDatabase),
         ResourceType = typeof(Package)),
     Versions("1.0.0")]
    public abstract class Package : SqlPackage
    {
        void StObjConstruct(CK.DB.Actor.Package actorPackage)
        {
        }
    }
}
